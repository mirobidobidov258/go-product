package apis

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

func HelloWorld(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello World endpoint hit")
}

func HadleFuntion() {
	router := mux.NewRouter()
	router.HandleFunc("/", HelloWorld)
	//Types
	router.HandleFunc("/types/create",CreateNewType).Methods("POST")
	router.HandleFunc("/types/update",UpdateType).Methods("PUT")
	//Category
	router.HandleFunc("/category/create",CreateNewCategory).Methods("POST")
	//Store
	router.HandleFunc("/store/{name}",CreatedNewStore).Methods("POST")
	//Address
	router.HandleFunc("/address/{district}/{street}/{store_id}",CreateNewAddress).Methods("POST")
	
	// Products
	router.HandleFunc("/products/create",CreateNewProducts).Methods("POST")
	
	


	fmt.Println("Server Listening...")
	if err := http.ListenAndServe("localhost:8181", router); err != nil {
		fmt.Println("Listen and Server error", err)
		return
	}
	
}
