package apis

import (
	"encoding/json"
	"io"
	"net/http"
	"temp/config"
	"temp/user"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func CreateNewAddress(w http.ResponseWriter, r *http.Request) {
	db, err := gorm.Open(postgres.Open(config.Dsn), &gorm.Config{})
	resError := user.Errors{}
	if err != nil {
		resError.ErrorCode = "500"
		resError.ErrorTitle = "Error while creating  database connection"
		json.NewEncoder(w).Encode(resError)
		return
	}
	requestBody, err := io.ReadAll(r.Body)
	if err != nil {
		resError.ErrorCode = "XXX"
		resError.ErrorTitle = "Error while accepting your info"
		json.NewEncoder(w).Encode(resError)
		return
	}
	var typeInfo []user.Address
	if err := json.Unmarshal(requestBody, &typeInfo); err != nil {
		resError.ErrorCode = "XXX"
		resError.ErrorTitle = "Error while accepting your info"
		json.NewEncoder(w).Encode(resError)
		return
	}
	if err := db.Create(&typeInfo).Error; err != nil {
		resError.ErrorCode = "XXX"
		resError.ErrorTitle = "Error while creating database"
		json.NewEncoder(w).Encode(resError)
		return
	}
	resError.ErrorCode = "200"
	resError.ErrorTitle = "New Address successfully created"
	json.NewEncoder(w).Encode(resError)
}

// func CreateNewAddress(w http.ResponseWriter, r *http.Request){
// 	db, err := gorm.Open(postgres.Open(config.Dsn),&gorm.Config{})
// 	if err != nil{
// 		fmt.Println("Error while creating connection with db",err)
// 		return
// 	}
// 	address := user.Address{}
// 	vars := mux.Vars(r)
// 	store_id, err := strconv.ParseUint(vars["store_id"], 10, 64)
// 	if err != nil{
// 		fmt.Println("Error cause STOREIDDD",err)
// 		return
// 	}
// 	address.District = vars["district"]
// 	address.Street = vars["street"]
// 	address.StoreID = uint(store_id)
// 	if err := db.Create(&address).Error; err != nil{
// 		fmt.Println("Error cause inserting to address table",err)
// 		return
// 	}
// 	fmt.Fprintf(w,"User successfully created")

// }
