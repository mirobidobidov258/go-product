package apis

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"temp/config"
	"temp/user"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func CreateNewType(w http.ResponseWriter, r *http.Request) {
	db, err := gorm.Open(postgres.Open(config.Dsn), &gorm.Config{})
	resError := user.Errors{}
	if err != nil {
		resError.ErrorCode = "500"
		resError.ErrorTitle = "Error while creating connection"
		json.NewEncoder(w).Encode(resError)
		return
	}
	requestBody, err := io.ReadAll(r.Body)
	if err != nil {
		resError.ErrorCode = "XXX"
		resError.ErrorTitle = "Error while accepting your info"
		json.NewEncoder(w).Encode(resError)
		return
	}
	var typeInfo []user.Types
	if err := json.Unmarshal(requestBody, &typeInfo); err != nil {
		resError.ErrorCode = "XXX"
		resError.ErrorTitle = "Error while accepting your info"
		json.NewEncoder(w).Encode(resError)
		return
	}
	if err := db.Create(&typeInfo).Error; err != nil {
		resError.ErrorCode = "XXX"
		resError.ErrorTitle = "Error while writing to database"
		json.NewEncoder(w).Encode(resError)
		return
	}
	resError.ErrorCode = "200"
	resError.ErrorTitle = "New Types successfully created"
	json.NewEncoder(w).Encode(resError)

}
func UpdateType(w http.ResponseWriter, r *http.Request){
	db, err := gorm.Open(postgres.Open(config.Dsn),&gorm.Config{})
	resError := user.Errors{}
	if err != nil{
		resError.ErrorCode = "500"
		resError.ErrorTitle = "Error deleting not found"
		json.NewEncoder(w).Encode(resError)
		return
	}
	var myType user.Types
	request, err := io.ReadAll(r.Body)
	if err != nil{
		resError.ErrorCode = "500"
		resError.ErrorTitle = "Error Read request json info"
		json.NewEncoder(w).Encode(resError)
		return
	}
	if err := json.Unmarshal(request, &myType); err != nil{
		resError.ErrorCode = "500"
		resError.ErrorTitle = "Error cause DON`T Unmarshalling"
		resError.ErrorInfo = fmt.Sprintf("%v",err)
		json.NewEncoder(w).Encode(resError)
		return
	}
	resType := user.Types{}
	db.Where("id= ?",myType.ID).Find(&resType)
	resType.Name = myType.Name
	if err := db.Save(&resType).Error; err != nil{
		resError.ErrorCode = "500"
		resError.ErrorTitle = "Error Saving info database"
		resError.ErrorInfo = fmt.Sprintf("%v",err)
		
		json.NewEncoder(w).Encode(resError)
		return
	}
	resError.ErrorCode = "200"
	resError.ErrorTitle = "Type successfully updated"
	resError.ErrorInfo = fmt.Sprintf("%v",err)
	
	json.NewEncoder(w).Encode(resError)



}




// func CreateNewType(w http.ResponseWriter, r *http.Request){
// 	db, err := gorm.Open(postgres.Open(config.Dsn),&gorm.Config{})
// 	if err != nil{
// 		fmt.Println("Error while creating connection with db",err)
// 		return
// 	}
// 	types := user.Types{}
// 	vars := mux.Vars(r)
// 	types.Name = vars["name"]

// 	if err := db.Create(&types).Error; err != nil{
// 		fmt.Println("Error while inserting to Type table",err)
// 		return
// 	}
// 	fmt.Fprintf(w, "User Succesfully created")
// }
