package apis

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"temp/config"
	"temp/user"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)
func CreateNewProducts(w http.ResponseWriter, r *http.Request) {
	db, err := gorm.Open(postgres.Open(config.Dsn), &gorm.Config{})
	resError := user.Errors{}
	if err != nil {
		resError.ErrorCode = "500"
		resError.ErrorTitle = "Error while creating database connection"
		json.NewEncoder(w).Encode(resError)
		return
	}

	products := []user.Products{}
	requestBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		resError.ErrorCode = "500"
		resError.ErrorTitle = "Error while getting your info"
		resError.ErrorInfo = fmt.Sprintf("%v",err)
		json.NewEncoder(w).Encode(resError)
		return
	}
	if err := json.Unmarshal(requestBody, &products); err != nil {
		resError.ErrorCode = "XXX"
		resError.ErrorTitle = "Error while accepting your info"
		resError.ErrorInfo = fmt.Sprintf("%v",err)
		json.NewEncoder(w).Encode(resError)
		return
	}
	tr := db.Session(&gorm.Session{SkipDefaultTransaction: true})
	if err := tr.Create(&products).Error; err != nil {
		resError.ErrorCode = "XXX"
		resError.ErrorTitle = "Error while writing to database"
		resError.ErrorInfo = fmt.Sprintf("%v",err)
		json.NewEncoder(w).Encode(resError)
		return
	}
	tr.Commit()
	resError.ErrorCode = "200"
	resError.ErrorTitle = "New products successfully created"
	json.NewEncoder(w).Encode(resError)
}
