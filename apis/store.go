package apis

import (
	"encoding/json"
	"io"
	"net/http"
	"temp/config"
	"temp/user"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func CreatedNewStore(w http.ResponseWriter, r *http.Request) {
	db, err := gorm.Open(postgres.Open(config.Dsn), &gorm.Config{})
	resError := user.Errors{}
	if err != nil {
		resError.ErrorCode = "500"
		resError.ErrorTitle = "Error while creating database connection"
		json.NewEncoder(w).Encode(resError)
		return

	}
	requestBody, err := io.ReadAll(r.Body)
	if err != nil {
		resError.ErrorCode = "XXX"
		resError.ErrorTitle = "Error while accepting your info"
		json.NewEncoder(w).Encode(resError)
		return
	}
	var typeInfo []user.Stores
	if err := json.Unmarshal(requestBody, &typeInfo); err != nil {
		resError.ErrorCode = "XXX"
		resError.ErrorTitle = "Error while accepting your info"
		json.NewEncoder(w).Encode(resError)
		return
	}
	if err := db.Create(&typeInfo).Error; err != nil {
		resError.ErrorCode = "XXX"
		resError.ErrorTitle = "Error while creating database"
		json.NewEncoder(w).Encode(resError)
		return
	}
	resError.ErrorCode = "200"
	resError.ErrorTitle = "New Store successfully created"
	json.NewEncoder(w).Encode(resError)
}
