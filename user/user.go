package user

import (
	"fmt"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type Types struct {
	gorm.Model
	Name    string   `json:"name"`
	Product Products `json:"-" gorm:"foreignKey:type_id"`
}

type Categories struct {
	gorm.Model
	Name    string   `json:"name"`
	Product Products `json:"-" gorm:"foreignKey:category_id"`
}
type Stores struct {
	gorm.Model
	Name      string     `json:"name"`
	Addressls []Address  `json:"address" gorm:"foreignKey:store_id;references:ID"`
	Product   []Products `json:"-" gorm:"many2many:stores_products;"`
}

type Address struct {
	gorm.Model
	District string `json:"district"`
	Street   string `json:"street"`
	StoreID  uint   `json:"-"`
}
type Products struct {
	gorm.Model
	Name       string   `json:"name"`
	CategoryID int      `json:"category_id"`
	TypeID     int      `json:"type_id"`
	Models     string   `json:"models"`
	Price      float64  `json:"price"`
	Amount     int      `json:"amount"`
	Stores     []Stores `json:"stores_list" gorm:"many2many:stores_products;"`
}
type Errors struct{
	ErrorCode string `json:"code"`
	ErrorTitle string `json:"title"`
	ErrorInfo string `json:"info"`
}
func InitialMigrations() {
	dsn := "host=localhost port=5432 user=citizenfour password=12321 dbname=goapi sslmode=disable"
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		fmt.Println("Error while creating connection", err)
		return
	}
	if err := db.AutoMigrate(&Types{}); err != nil {
		fmt.Println("Error while creating Types table", err)
		return
	}
	if err := db.AutoMigrate(&Categories{}); err != nil {
		fmt.Println("Error while creating Category table", err)
		return
	}
	if err := db.AutoMigrate(&Stores{}); err != nil {
		fmt.Println("Error while creating Stores table", err)
		return
	}
	if err := db.AutoMigrate(&Address{}); err != nil {
		fmt.Println("Error while creating Address table", err)
		return
	}
	if err := db.AutoMigrate(&Products{}); err != nil {
		fmt.Println("Error while creating Products table", err)
		return
	}

}
